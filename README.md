# Weekday Assignment

Overview of weekday project.

* Installing 
* Clone the repository

```
git clone <repository-url>
```

* Navigate to the project directory
```
cd project-directory
```

* Install dependencies

```
npm install

```

## Usage

Run the project by running the command
```
npm run dev
```


