import React, { useContext, useEffect } from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import DashboardContext from './DashboardContext';

import {
  Box,
  Toolbar,
  Typography,
  IconButton,
  List,
  ListItemText,
  ListItemIcon,
  ListItem,
  ListItemButton,
  Grid,
  Popover,
  Avatar,
  Card,
  Dialog,
  Badge,
} from '@mui/material';

import { useState } from 'react';

import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';

import useStyles from '../../components/styles';

import {
  iconConstants,
  linkConstants,
  navigationContants,
  typographyConstants,
} from '../../resources/theme/constants';

import { getTablewidth } from '../../components/alignmentLayout';
import CustomIcon from '../../components/CustomIcon';
import RouterBreadcrumbs from '../../components/RouterBreadcrumbs';

import { ToastContext } from '../../components/ToastContext';
import { startTransition } from 'react';


const drawerWidth = 220;

const StyledListItem = styled((props) => <ListItem {...props} />)(
  ({ theme }) => ({
    display: 'flex',
    height: '64px',
    '&.Mui-selected': {
      borderRight: '3px solid',
      borderRightColor: theme.palette.sideBarColors.main,
      backgroundColor: theme.palette.sideBarColors.selectBackground,
    },
    '&:hover': {
      backgroundColor: theme.palette.sideBarColors.selectBackground,
    },
  })
);

const MUIAppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  justifyContent: 'center',

  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
  ...(!open && {
    marginLeft: '100px',
    width: `calc(100% - 100px)`,
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  ...theme.mixins.toolbar,
}));

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: '85px',
});

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  width: drawerWidth,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',

  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: theme.palette.globalElementColors.greenNew,
    padding: '3px 4px',
    height: '25px',
    borderRadius: '5px',
    top: '-5px',
    right: '-5px',
  },
}));

export const ShowToast = function () {
  const { showToast } = useContext(ToastContext);
  return showToast;
};

const Dashboard = () => {
//   const user = getUserFromLocalStorage();
    const user = {
        profile: {
            fullName: 'Zaid Ahmed',
            email: 'mailtozaid09@gmail.com'
        }
    };
  const authToken = user?.authToken;
  const companyId = user?.profile?.companyId;
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();

  const [screenWidth, setScreenWidth] = useState(null);

  const [profileData, setProfileData] = useState(user);
  const [uploadFormOpen, setUploadFormOpen] = useState(false);
  const [inputFileObject, setInputFileObject] = useState({
    fileName: '',
    fileUrl: '',
    file: '',
    extensionError: false,
  });

  const [outputFileObject, setOutputFileObject] = useState({
    fileName: '',
    fileUrl: '',
    file: '',
    extensionError: false,
  });

    const [miscellaneous, setMiscellaneous] = useState('');

    const pathname = window.location.pathname;
    const selectIndex =
        pathname === '/applied-jobs' ? linkConstants.link2 
        : pathname === '/search-jobs' ? linkConstants.link3 
        : pathname === '/search-salary' ? linkConstants.link4 
        : pathname === '/referral' ? linkConstants.link5
        : pathname === '/recommendation' ? linkConstants.link6 
        : pathname === '/share-extension' ? linkConstants.link6 
        : linkConstants.link1

  const [selectedIndex, setSelectedIndex] = useState(selectIndex);

  const [changePasswordCardOpen, setChangePasswordCardOpen] = useState(false);
  const [drawerOpened, setDrawerOpen] = useState(true);
  const [sideMenuWidth, setSideMenuWidth] = useState('285px');
  const [tableWidth, setTableWidth] = useState(
    `calc(100vw - ${sideMenuWidth})`
  );

  
  const [anchorEl, setAnchorEl] = useState(null);
  const [settingAnchorEl, setSettingAnchorEl] = useState(null);

//   const [loader, showLoader, hideLoader] = UseLoader();
  const { showToast } = useContext(ToastContext);
  const anchorOrigin = { vertical: 'bottom', horizontal: 'right' };
  
  const [appBarBorderBottom, setAppBarBorderBottom] = useState(false);

  useEffect(() => {

  }, []);

  useEffect(() => {
    pathname === '/fetch-receipt'
      ? setAppBarBorderBottom(true)
      : setAppBarBorderBottom(false);
    setSelectedIndex(
        pathname === '/search-jobs' ? linkConstants.link2
        : linkConstants.link1
    );
  }, [pathname]);

  useEffect(() => {
    outputFileObject.fileName !== '' &&
      outputFileObject.fileUrl !== '' &&
      outputFileObject.file !== '' &&
      profileUpload(false);
  }, [outputFileObject]);

  const handleEditProfileImageClick = (e) => {
    setInputFileObject({
      ...inputFileObject,
      file: outputFileObject.file,
      fileUrl: outputFileObject.fileUrl,
      fileName: outputFileObject.fileName,
    });
    setUploadFormOpen(true);
  };


  const handleSignOut = (e) => {
    // e.preventDefault();
    // signOut();
  };

  const handleProfileClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleProfileSettingClick = (event) => {
    setSettingAnchorEl(event.currentTarget);
  };

  const handleSettingClose = () => {
    setSettingAnchorEl(null);
  };

  const profilePopupOpen = Boolean(anchorEl);
  const id = profilePopupOpen ? 'simple-popover' : undefined;
  const profileSettingsOpen = Boolean(settingAnchorEl);
  const seetingId = profileSettingsOpen ? 'simple-Seetings-popover' : undefined;

  const handleDrawer = () => {
    setDrawerOpen(!drawerOpened);
  };

  useEffect(() => {
    changeSideMenuWidth();
    updateTableWidth();
  }, [drawerOpened, sideMenuWidth]);

  const closeDrawer = () => {
    let scaleVariable = window.screen.availWidth / window.innerWidth;
    if (scaleVariable >= 1.75) {
      setDrawerOpen(false);
    }
  };

  useEffect(() => {
    window.addEventListener('resize', updateTableWidth);
    return () => window.removeEventListener('resize', updateTableWidth);
  });
  useEffect(() => {
    window.addEventListener('resize', closeDrawer);
    return () => window.removeEventListener('resize', closeDrawer);
  });

  const updateTableWidth = () => {
    if(window.innerWidth < 1024) setDrawerOpen(false)
    setScreenWidth(window.innerWidth)
    setTableWidth(getTablewidth(sideMenuWidth, drawerOpened));
  };

  const changeSideMenuWidth = () => {
    if (drawerOpened == false) {
      setSideMenuWidth('179px');
    } else {
      setSideMenuWidth('285px');
    }
  };

  const handleListItemClick = (index) => {
    setAppBarBorderBottom(false);
    setSelectedIndex(index);

    if (index === linkConstants.link1) {
      navigate(navigationContants.dashboard);
    } else if (index === linkConstants.link2) {
      navigate(navigationContants.searchJobs);
      if (location.pathname === '/search-jobs') window.location.reload();
    } else if (index === linkConstants.link3) {
        navigate(navigationContants.searchSalary);
        if (location.pathname === '/search-salary') window.location.reload();
    } else if (index === linkConstants.link4) {
        navigate(navigationContants.referral);
        if (location.pathname === '/referral') window.location.reload();
    } else if (index === linkConstants.link5) {
        navigate(navigationContants.recommendation);
        if (location.pathname === '/recommendation') window.location.reload();
    } else if (index === linkConstants.link6) {
        navigate(navigationContants.shareExtension);
        setAppBarBorderBottom(true);
        if (location.pathname === '/share-extension') window.location.reload();
      } 
  };

  return (
    <DashboardContext.Provider
      value={{
        drawerOpened,
        setDrawerOpen,
      }}
    >
      <Box sx={{ display: 'flex' }}>
        <MUIAppBar
          open={drawerOpened}
          sx={{
            backgroundColor: 'appbar.background',
            boxShadow: 'none',
            height: 89,
            padding: '0px 32px',
            borderBottom: '1px solid',
            borderBottomColor: 'globalElementColors.canvas2'
          }}
        >
          <Toolbar disableGutters >
            <Grid
              container
              sx={{ display: 'flex', direction: 'row', width: '100%' }}
            >
              <Grid item xs className={classes.flexcenterJustifyFlexStart} >
                <Typography variant={typographyConstants.subtitle1} sx={{fontSize: '24px'}} >
                  {/* <RouterBreadcrumbs /> */}
                  👋 Zaid
                </Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </MUIAppBar>
        <Drawer
          PaperProps={{
            sx: {
              boxShadow: 'var(--shadow-3)',
            },
          }}
          variant='permanent'
          open={drawerOpened}
          onClose={handleDrawer}
        >
          <DrawerHeader
            sx={{ height: 120,  }}
            className={classes.flexColumnCenter}
          >
            <Box sx={{ pb: '8px' }}>
                {drawerOpened
                ?
                <CustomIcon
                  name={iconConstants.weekdayLogo}
                  style={{
                    height: '43px',
                    width: '155px',
                  }}
                  svgStyle={'height: 63px; width: 95px;'}
                />
                :
                <CustomIcon
                  name={iconConstants.weekdayLogoIcon}
                  style={{
                    height: '53px',
                    width: '53px',
                  }}
                  svgStyle={'height: 63px; width: 95px;'}
                />
                }
                
            </Box>

            {screenWidth > 1024 && (<IconButton
              sx={{
                position: 'fixed',
                left: drawerOpened ? '190px' : '55px',
                top: '65px',
                transition: 'left 0.24s ease-in-out',
                zIndex: (theme) => theme.zIndex.drawer + 1,
              }}
              onClick={handleDrawer}
            >
              {drawerOpened ? (
                <Box sx={{background: 'white', borderRadius: 2, border: '1.5px solid lightgrey'}} >
                    <CustomIcon
                        name={iconConstants.leftArrow}
                        style={{
                            height: '30px',
                            width: '30px',
                        }}
                    />
                </Box>
              ) : (
                <Box sx={{background: 'white', borderRadius: 2, border: '1.5px solid lightgrey'}} >
                    <CustomIcon
                      name={iconConstants.rightArrow}
                      style={{
                        height: '30px',
                        width: '30px',
                      }}
                    />
                </Box>
              )}
            </IconButton>)}
          </DrawerHeader>

          <List
            component='nav'
            sx={{
              pt: '0px',
              paddingBottom: '0px',
              background: 'sideBar.main',
            }}
          >
            <Box sx={{alignItems: 'center',  borderTop: '1px solid #dddddd', p: '10px', display: 'flex', justifyContent: drawerOpened ? 'flex-start' : 'center', }} >
                <Typography variant={typographyConstants.subtitle2} sx={{ color: 'textColors.main', ml: drawerOpened ? 1 : '',}}>
                    {drawerOpened ? 'Looking for job' : 'Get Jobs'}
                </Typography>
            </Box>
            {[
              linkConstants.link1,
              linkConstants.link2,
              linkConstants.link3,
              linkConstants.link4
            ].map((text, index) => (
              <StyledListItem
                key={text}
                disablePadding
                selected={selectedIndex === text}
                onClick={() => handleListItemClick(text)}
              >
                <ListItemButton
                  sx={{
                    height: 64,
                    justifyContent: drawerOpened ? 'initial' : 'center',
                    padding: drawerOpened ? '20px 16px' : '20px 45px',
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: drawerOpened ? 3 : 'auto',
                      justifyContent: 'center',
                    }}
                  >
                    {index === 0 &&
                      (<CustomIcon
                        name={iconConstants.appliedJobs}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                    {index === 1 &&
                      (<CustomIcon
                        name={iconConstants.searchJobs}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                     {index === 2 &&
                      (<CustomIcon
                        name={iconConstants.searchSalary}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                    {index === 3 &&
                      (<CustomIcon
                        name={iconConstants.referral}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                  </ListItemIcon>
                  <ListItemText
                    primary={
                      <Typography
                        variant={
                          selectedIndex === text
                            ? typographyConstants.subtitle1
                            : typographyConstants.subtitle2
                        }
                        sx={{
                          color:
                            selectedIndex === text ? 'textColors.main' : '',
                        }}
                      >
                        {text}
                      </Typography>
                    }
                    sx={{ opacity: drawerOpened ? 1 : 0 }}
                  />
                </ListItemButton>
              </StyledListItem>
            ))}
          </List>


          <List
            component='nav'
            sx={{
              pt: '0px',
              paddingBottom: '0px',
              background: 'sideBar.main',
            }}
          >
            <Box style={{}} />
             <Box sx={{alignItems: 'center', borderTop: '1px solid #dddddd', p: '10px', pt: '20px', display: 'flex', justifyContent: drawerOpened ? 'flex-start' : 'center', }} >
                <Typography variant={typographyConstants.subtitle2} sx={{ color: 'textColors.main', ml: drawerOpened ? 1 : '', }}>
                    {drawerOpened ? 'Recommend and earn' : 'Refer'}
                </Typography>
            </Box>
            {[
              linkConstants.link5,
              linkConstants.link6
            ].map((text, index) => (
              <StyledListItem
                key={text}
                disablePadding
                selected={selectedIndex === text}
                onClick={() => handleListItemClick(text)}
              >
                <ListItemButton
                  sx={{
                    height: 64,
                    justifyContent: drawerOpened ? 'initial' : 'center',
                    padding: drawerOpened ? '20px 16px' : '20px 45px',
                  }}
                >
                  <ListItemIcon
                    sx={{
                      minWidth: 0,
                      mr: drawerOpened ? 3 : 'auto',
                      justifyContent: 'center',
                    }}
                  >
                    {index === 0 &&
                      (<CustomIcon
                        name={iconConstants.recommend}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                    {index === 1 &&
                      ( <CustomIcon
                        name={iconConstants.share}
                        style={{ height: '24px', width: '24px' }}
                      />)}
                  </ListItemIcon>
                  <ListItemText
                    primary={
                      <Typography
                        variant={
                          selectedIndex === text
                            ? typographyConstants.subtitle1
                            : typographyConstants.subtitle2
                        }
                        sx={{
                          color:
                            selectedIndex === text ? 'textColors.main' : '',
                        }}
                      >
                        {text}
                      </Typography>
                    }
                    sx={{ opacity: drawerOpened ? 1 : 0 }}
                  />
                </ListItemButton>
              </StyledListItem>
            ))}
          </List>
        </Drawer>

        <Box component='main' sx={{ flexGrow: 1 }}>
          <Toolbar sx={{ height: 89 }} />

          <Outlet />
        </Box>
      </Box>
      {/* {loader} */}
    </DashboardContext.Provider>
  );
};

export default Dashboard;
