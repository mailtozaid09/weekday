import React, { useState, useEffect } from 'react';
import { Box, Typography } from '@mui/material';

import { getAllJobs } from '../../services/apiRequests';
import JobsCard from '../../components/JobsCard';

import Lottie from 'lottie-react';
import LoadingJson from '../../resources/images/svg_image/Loading.json';
import { filters } from '../../services/sampleData';

import CustomMultiSelect from '../../components/CustomMultiSelect';
import CustomSelect from '../../components/CustomSelect';
import CustomSearchInput from '../../components/CustomSearchInput';
import { iconConstants, typographyConstants } from '../../resources/theme/constants';
import CustomIcon from '../../components/CustomIcon';

const SearchJobs = () => {
    const [allJobs, setAllJobs] = useState([]);
    const [page, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(false);

    const [roles, setRoles] = useState(filters.roles);
    const [employees, setEmployees] = useState(filters.num_of_emp);
    const [experience, setExperience] = useState(filters.experience);
    const [jobType, setJobType] = useState(filters.jobType);
    const [minimumSalary, setMinimumSalary] = useState(filters.minimumSalary);

    const [selectedRoles, setSelectedRoles] = useState([]);
    const [selectedEmp, setSelectedEmp] = useState([]);
    const [selectedExp, setSelectedExp] = useState([]);
    const [selectedJobType, setSelectedJobType] = useState([]);
    const [selectedMinSalary, setSelectedMinSalary] = useState([]);

    const [companyName, setCompanyName] = useState('');

    const [jobFilters, setJobFilters] = useState({
        roles: [],
        jobType: [],
        employees: '',
        minimumSalary: '',
        experience: '',
        companyName: ''
    });

    useEffect(() => {
        fetchData();
    }, []);


    useEffect(() => {
        fetchData()
        console.log("selectedRoles> > ",selectedRoles);
        console.log("selectedEmp> > ",selectedEmp);
        console.log("selectedExp> > ",selectedExp);

        console.log("selectedJobType> > ",selectedJobType);
        console.log("selectedMinSalary> > ",selectedMinSalary);
        console.log("companyName> > ",companyName);
    }, [selectedRoles, selectedEmp, selectedExp, selectedJobType, selectedMinSalary, companyName])
    

    useEffect(() => {
        const handleScroll = () => {
            const scrollTop = document.documentElement.scrollTop;
            const windowHeight = window.innerHeight;
            const documentHeight = document.documentElement.offsetHeight;

            if (scrollTop + windowHeight >= documentHeight - 50 && !isLoading) {
            
            setIsLoading(true);
            setTimeout(() => {
                
                fetchData();
            }, 3000); 
            }
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [isLoading]);

    const fetchData = async () => {
        setIsLoading(true);
        try {
        const body = {
            limit: 20,
            offset: page
        };

        const onSuccess = (resp) => {
            console.log("alljobs resp > ", resp?.data);


            filterDataFunction(resp?.data?.jdList)

            if(!selectedRoles && !selectedEmp && !selectedExp && !selectedJobType && selectedMinSalary){
                setAllJobs((prevJobs) => [...prevJobs, ...resp?.data?.jdList]);
            }
            setPage((prevPage) => prevPage + 1);
            setIsLoading(false);
        };
        const onError = (err) => {
            console.log("alljobs err > ", err);
            setIsLoading(false);
        };

        await getAllJobs(body, onSuccess, onError);
        } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
        }
    };
    const filterDataFunction = (data) => {
        const lowerCasedRoles = selectedRoles.map(item => item.toLowerCase());
        const lowerCasedJobType = selectedJobType.map(item => item.toLowerCase());
    
        const isRoleFilterApplied = lowerCasedRoles.length > 0;
        const isJobTypeFilterApplied = lowerCasedJobType.length > 0;
    
        const filteredListings = data.filter(listing => {
            const roleMatches = !isRoleFilterApplied || lowerCasedRoles.includes(listing.jobRole.toLowerCase());
            const jobTypeMatches = !isJobTypeFilterApplied || lowerCasedJobType.includes(listing.location.toLowerCase());
    
            return roleMatches && jobTypeMatches;
        });
    
        setAllJobs(filteredListings);

        console.log("filteredListings > > ", filteredListings);
        filteredListings?.forEach(item => {
            console.log("item > > ", item.location);
        });
    }
    

    const handleRolesChange = (event) => {
        setSelectedRoles(event.target.value);
    };

    const handleRoleDelete = (value) => {
        if(value == 'clear'){
            setSelectedRoles([])
        }else{
            setSelectedRoles((prevSelectedRoles) =>
                prevSelectedRoles.filter((selectedRole) => selectedRole !== value)
            );
        }
    };

    const handleJobTypeChange = (event) => {
        setSelectedJobType(event.target.value);
    };

    const handleJobTypeDelete = (value) => {
        if(value == 'clear'){
            setSelectedJobType([])
        }else{
            setSelectedJobType((prevSelectedJobType) =>
                prevSelectedJobType.filter((selectedJob) => selectedJob !== value)
            );
        }
    };

    

  return (
    <Box sx={{p: '20px'}} >

        <Box sx={{mt: 2, mb: 6, display: 'flex', gap: 2, flexDirection: 'row', flexWrap: 'wrap'}} >
            <CustomMultiSelect 
                title="Select Roles"
                dataArray={roles} 
                selectedValue={selectedRoles}
                handleChange={handleRolesChange}
                handleDelete={handleRoleDelete}
            />

            <CustomSelect
                title="Number of employees" 
                dataArray={employees} 
                minWidth={150} 
                selectedValue={selectedEmp}
                handleChange={(e) => setSelectedEmp(e.target.value)}
                handleDelete={() => setSelectedEmp('')}
            />

            <CustomSelect 
                title="Experience" 
                dataArray={experience} 
                minWidth={100} 
                selectedValue={selectedExp}
                handleChange={(e) => setSelectedExp(e.target.value)}
                handleDelete={() => setSelectedExp('')}
            />

            <CustomMultiSelect 
                title="Job type" 
                dataArray={jobType}
                minWidth={140}
                selectedValue={selectedJobType}
                handleChange={handleJobTypeChange}
                handleDelete={handleJobTypeDelete}
            /> 

            <CustomSelect 
                title="Minimun Base Pay" 
                dataArray={minimumSalary} 
                minWidth={140} 
                selectedValue={selectedMinSalary}
                handleChange={(e) => setSelectedMinSalary(e.target.value)}
                handleDelete={() => setSelectedMinSalary('')}
            />

            

            <CustomSearchInput 
                title="Company Name" 
                value={companyName}
                onChange={(e) => setCompanyName(e.target.value)}
            /> 
        </Box>

        <div id="data-container">
            <Box className="grid-container">
                {allJobs?.map((item, index) => (
                    <JobsCard key={index} index={index} sx_style={{}} data={item} />
                ))}
            </Box>

            {allJobs?.length == 0 && (
            <Box sx={{display: 'flex', height: 200, flexDirection: 'column', alignItems: 'center', justifyContent: 'center',}} >
                    {/* <CustomIcon
                        name={iconConstants.nothingFound}
                        svgStyle={'height: 63px; width: 95px;'}
                    /> */}
                <Typography variant={typographyConstants.callout1} sx={{ width: 350, alignItems: 'center', justifyContent: 'center', display: 'flex'}} >
                    No Jobs available for this category at the moment
                </Typography>
            </Box>)}

            {isLoading && (
                <Box sx={{height: 100, display: 'flex', alignItems: 'center', justifyContent: 'center'}} >
                    <Lottie 
                        animationData={LoadingJson}
                        style={{height: 100, width: 100}}
                    />
                </Box>
            )}
        </div>
    </Box>
  );
};


export default SearchJobs
