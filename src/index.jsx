import React from 'react';
import ReactDOM from 'react-dom/client';

import 'react-datepicker/dist/react-datepicker.css';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import '../src/resources/fonts/fonts.css';
import '../src/resources/CSS/index.css';
import '../src/components/custom_style.css';

import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <>
    <App />
  </>
);