export const capitalizeSentence = (sentence) => {
    // Split the sentence into words
    const words = sentence?.split(' ');
    const capitalizedWords = words?.map(word => {
        return word?.charAt(0).toUpperCase() + word?.slice(1);
    });
    return capitalizedWords?.join(' ');
}


export const getSalaryRange = (minJdSalary, maxJdSalary, salaryCurrencyCode = 'USD') => {

    var salary 

    if(minJdSalary && maxJdSalary){
        salary = `${minJdSalary} - ${maxJdSalary} ${salaryCurrencyCode}`;
    }else if(!minJdSalary){
        salary = `Upto ${maxJdSalary} ${salaryCurrencyCode}`;
    }else if(!maxJdSalary){
        salary = `Upto ${minJdSalary} ${salaryCurrencyCode}`;
    }

    return salary
}


export const isPrimeNumber = (num) => {
    if (num <= 1) return false;
    if (num === 2) return true;
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) return false;
    }
    return true;
}


export const getRandomNumber = (min = 1, max = 31) => {
    var randomDay = Math.floor(Math.random() * (max - min + 1)) + min;
    return randomDay
}
