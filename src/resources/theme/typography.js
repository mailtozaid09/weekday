import { createTheme } from '@mui/material';
import { colors } from './colors';

const fontFamilySGreg = '"LexendRegular", "sans-serif"';
const fontFamilySGmed = '"LexendMedium", "sans-serif"';
const fontFamilySGsemi = '"LexendSemiBold", "sans-serif"';
const fontFamilyMSreg = '"LexendRegular", "sans-serif"';
const fontFamilyMSmed = '"LexendMedium", "sans-serif"';
const fontFamilyMSsemi = '"LexendSemiBold", "sans-serif"';

const fontFamilyMSBold = '"LexendBold", "sans-serif"';

const fontFamilySSPreg = '"LexendRegular", "sans-serif"';

export const typography = {
  typography: {
    fontFamily: fontFamilySGreg,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    color: colors.blue1,

    //headline1 (in figma - this typlography is named as headline1 in figma design)
    h1: {
      fontFamily: fontFamilySGmed,
      fontSize: '4.75rem', //76px med
      fontWeight: 500,
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },
    //headline2 (in figma)
    h2: {
      fontFamily: fontFamilySGreg,
      fontSize: '4rem', //64px reg
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    //headline3 (in figma)
    h3: {
      fontFamily: fontFamilySGmed,
      fontSize: '3rem', //48px med
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },
    //headline4 (in figma)
    h4: {
      fontFamily: fontFamilySGmed,
      fontSize: '2.375rem', //38px med
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },
    //title1 (in figma)
    h5: {
      fontFamily: fontFamilySGmed,
      fontSize: '1.5rem', //24px med
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },
    //title2 (in figma)
    h6: {
      fontFamily: fontFamilyMSmed,
      fontSize: '1.25rem', //20px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },
    //title3 (in figma)
    title3: {
      fontFamily: fontFamilySGreg,
      fontSize: '1.25rem', //20px reg
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    subtitle1: {
      fontFamily: fontFamilySGreg,
      fontSize: '0.925rem', //18px med
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    subtitle2: {
      fontFamily: fontFamilySGreg,
      fontSize: '0.925rem', //18px reg
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    subtitle3: {
      fontFamily: fontFamilySGreg,
      fontSize: '1rem', //16px med
      fontWeight: 500,
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    body1: {
      fontFamily: fontFamilySGreg,
      fontSize: '1rem', //16px reg
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '-0.05em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    body2: {
      fontFamily: fontFamilyMSmed,
      fontSize: '0.875rem', //14px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    body3: {
      fontFamily: fontFamilyMSreg,
      fontSize: '0.875rem', //14px reg
      lineHeight: '160%',
      fontWeight: 400,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    callout1: {
      fontFamily: fontFamilyMSmed,
      fontSize: '1.25rem', //20px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    callout2: {
      fontFamily: fontFamilyMSmed,
      fontSize: '0.875rem', //14px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    
    callout3: {
        fontFamily: fontFamilyMSsemi,
        fontSize: '1rem', //20px med
        lineHeight: '160%',
        fontWeight: 500,
        fontStyle: 'normal',
        letterSpacing: '0.01em',
        color: colors.blue1,
        wordBreak: 'break-word',
      },

    caption: {
      fontFamily: fontFamilyMSsemi,
      fontSize: '0.75rem', //12px semi
      lineHeight: '160%',
      fontWeight: 600,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
      wordBreak: 'break-word',
    },

    button: {
      fontFamily: fontFamilyMSmed,
      fontSize: '0.875rem', //14px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      textTransform: 'none',
      wordBreak: 'break-word',
    },

    testFont: {
      fontFamily: fontFamilySSPreg,
      fontSize: '0.875rem', //14px med
      lineHeight: '160%',
      fontWeight: 500,
      fontStyle: 'normal',
      letterSpacing: '0.01em',
      color: colors.blue1,
    },
  },
};
