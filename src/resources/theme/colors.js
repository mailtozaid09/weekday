export const colors = {
  green: '#00E600',
  greenNew: '#55efc4',
  green1: '#5EB261',
  green2: '#B9F8B9',
  green3: '#E8FDE8',
  green4: '#D1FAD1',

  blue1: '#1D1D2C',
  blue2: '#12121C',
  blue3: '#1A1A28',
  blue4: '#323340',
  cardBase: '#282836',

  gray1: '#D6D6D9',
  gray2: '#C1C1C5',
  gray3: '#ADADB2',
  gray4: '#84848C',

  text1: '#D6D6D9',
  text2: '#53545D',
  white: '#ffffff',

  alert1: '#E05959',
  alert2: '#FFA39E',
  alert3: '#FFF1F0',

  canvas1: '#EAEAEC',
  canvas2: '#F1F0F5',

  //do not use bellow colors

  delightfulGreen: '#00e600',
  greyBody: '#F1F0F5',
  blue1: '#1D1D2C',
  color: '#53545D',

  buttonHover: '#1A1A28',
  disabledGrey: '#EAEAEC',
  disabledText: '#ADADB2',
  nuetralsgray3: '#ADADB2',

  color1: '#333333',
  color2: '#ffffff',
  color3: '#ff0000',
  color4: 'rgba(194, 202, 237, 0.5)',
  color5: '#acb5de',
  color6: '#33333366',
  color7: '#eef1f4',

  colorViolet: '#ACB5DE',
  colorGrey: '#66708066',
  colorGreyDark: '#667080',
  colorgray2: '#EFF0F7',
  colorgray3: '#E3E5F0',
  colorgray4: '#EAECF6',
  colorLightGrey: '#F9FAFD',
  colorLightgray2: '#33333330',
  colorLightGreyish: 'rgba(242, 244, 251, 1)',
  colorMediumGrey: 'rgba(175, 181, 217, 0.37)',
  colorLightgray3: '#d9e2ec',
  colorLightgray4: '#F6F8FC',
  colorLightGrey5: '#C2CAED29',
  colorLightGrey6: '#EEF0FF',
  colorDarkViolet: '#6F77A7',
  colorLightViolet: '#C2CAED7D',
  colorLightViolet2: '#C2CAED24',
  colorLightBlack: '#33333399',
  colorBlack2: '#0000004d',

  colorWhite: '#ffffff',
  headColor: '#EEF1F4',
};
