import { createTheme } from '@mui/material';
import { colors } from './colors';

const { palette } = createTheme();

export const light = {
  palette: {
    mode: 'light',
    background: {
      default: colors.canvas2,
    },

    globalElementColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        green1: colors.green1,
        greenNew: colors.greenNew,
        white: colors.white,
        gray1: colors.gray1,
        canvas1: colors.canvas1,
        canvas2: colors.canvas2,
        gray2: colors.gray2,
        gray3: colors.gray3,
        alert1: colors.alert1,
      },
    }),

    textColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        text1: colors.text1,
        text2: colors.text2,
        alert: colors.alert1,
        grey: colors.gray4,
        greenNew: colors.greenNew,
      },
    }),

    background: palette.augmentColor({
      color: { main: colors.canvas2, white: colors.white },
    }),

    inputFieldColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        border: colors.gray3,
        borderFocus: colors.blue1,
        errorColor: colors.alert1,
        background: colors.white,
      },
    }),

    inputLabelColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        contrastText: colors.gray3,
        labelHoverText: colors.blue1,
        labelFilled: colors.blue1,
        contrastText2: colors.text2,
      },
    }),

    buttonColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        background1: colors.blue1,
        background2: colors.greenNew,
        hover1: colors.blue3,
        hover2: colors.green,
        contrastText1: colors.white,
        contrastText2: colors.blue1,
        disabledButtonBackground: colors.canvas1,
        disabledButtonText: colors.gray3,
        cancelButtonBackground: colors.canvas1,
        cancelButtonText: colors.blue1,
        cancelButtonHover: colors.canvas1,
        delete: colors.color3,
      },
    }),

    sideBarColors: palette.augmentColor({
      color: {
        main: colors.greenNew,
        rightBorder: colors.greenNew,
        selectBackground: colors.canvas2,
        contrastText: colors.blue1,
      },
    }),

    searchBarColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        labelText: colors.gray3,
        background: colors.white,
        borderColor: colors.gray1,
        borderFocus: colors.gray3,
      },
    }),

    filterButtonColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        contrastText: colors.blue1,
        background: colors.white,
        borderColor: colors.gray1,
        borderFocus: colors.gray3,
      },
    }),

    tableHeaderColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        background: colors.blue4,
        contrastText: colors.white,
        background2: colors.canvas2,
      },
    }),

    tableBodyColors: palette.augmentColor({
      color: {
        main: colors.blue1,
        background: colors.white,
        border: colors.canvas2,
        contrastText: colors.blue1,
        rowHover: colors.canvas1,
        statusActive: colors.green1,
        statusInvited: colors.canvas1,
        statusInavtive: colors.alert1,
        statusBorder: colors.gray1,
        alert: colors.alert1,

        statusLive: colors.greenNew,
        statusDraft: colors.gray3,
        statusUpcoming: colors.gray4,
        statusPaused: colors.alert2,
        statusExpired: colors.alert1,
      },
    }),

    appbar: palette.augmentColor({
      color: {
        main: colors.green,
        background: colors.white,
      },
    }),

    dialogPopup: palette.augmentColor({
      color: {
        main: colors.green,
        headerBgColor: colors.canvas2,
        buttonColor: colors.blue1,
        buttonTextColor: colors.white,
      },
    }),

    cardColors: palette.augmentColor({
      color: {
        main: colors.white,
        border: colors.canvas2,
        alert: colors.alert1,
        borderColor: colors.gray2,
        backgroundColor: colors.white,
      },
    }),

    box: palette.augmentColor({
      color: {
        main: colors.white,
        borderColor: colors.text2,
      },
    }),

    // don't use below items - Old Colors

    tableContainer: palette.augmentColor({
      color: { main: colors.color2, tableStatusbg: colors.colorMediumGrey },
    }),
    tableHead: palette.augmentColor({ color: { main: colors.color4 } }),
    tableViewHead: palette.augmentColor({
      color: { main: colors.colorLightViolet2 },
    }),
    uploadImage: palette.augmentColor({
      color: { main: colors.color4, border: colors.color5 },
    }),
    // tableViewRow: palette.augmentColor({
    //   color: { main: colors.color2, state: colors.colorLightGreyish },
    // }),
    tableViewcell: palette.augmentColor({
      color: { main: colors.colorgray2 },
    }),

    inputLabel: palette.augmentColor({
      color: {
        main: colors.colorViolet,
        contrastText: colors.colorGrey,
        labelHoverText: colors.color1,
      },
    }),
    listItem: palette.augmentColor({
      color: {
        main: colors.colorDarkViolet,
        contrastText: colors.colorLightViolet,
        borderRight: colors.greenNew,
        hoverBg: colors.canvas2,
      },
    }),
    sideBar: palette.augmentColor({
      color: {
        main: colors.colorLightGrey,
      },
    }),
    filterBox: palette.augmentColor({
      color: {
        main: colors.colorLightgray2,
      },
    }),

    filterList: palette.augmentColor({
      color: {
        main: colors.colorLightGrey5,
      },
    }),
    card: palette.augmentColor({
      color: {
        main: colors.colorLightGrey,
        border: colors.colorgray3,
        bg2: colors.colorgray2,
        borderLeft: colors.color1,
        typoBg: colors.colorgray3,
        footerBg: colors.colorgray3,
        footerbg2: colors.colorgray4,
        borderBetween: colors.colorLightGrey6,
      },
    }),
    selectStyle: palette.augmentColor({
      color: {
        main: colors.color2,
        typoLabel: colors.colorLightBlack,
      },
    }),
    textField: palette.augmentColor({
      color: {
        main: colors.colorGrey,
        typoLabel: colors.colorLightBlack,
        textLabel: colors.colorLightBlack,
        background: colors.color2,
        borderColor: colors.color1,
      },
    }),

    sidebarProfile: palette.augmentColor({
      color: {
        main: colors.colorgray2,
      },
    }),
    toast: palette.augmentColor({
      color: {
        main: colors.colorBlack2,
        fontColor: colors.color1,
        borderColor: colors.colorDarkViolet,
      },
    }),
    backDrop: palette.augmentColor({
      color: {
        main: colors.color2,
      },
    }),
    tabs: palette.augmentColor({
      color: {
        main: colors.colorLightGrey,
        tabIndicator: colors.color1,
      },
    }),
  },
};

export const dark = {
  palette: {
    mode: 'dark',
  },
};
