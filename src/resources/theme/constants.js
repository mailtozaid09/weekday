export const typographyConstants = {
  callout1: 'callout1',
  callout2: 'callout2',
  callout3: 'callout3',
  body1: 'body1',
  body2: 'body2',
  body3: 'body3',
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
  title3: 'title3',
  subtitle1: 'subtitle1',
  subtitle2: 'subtitle2',
  subtitle3: 'subtitle3',
  caption: 'caption',
};

export const linkConstants = {
    link1: 'My Applied Jobs',
    link2: 'Search Jobs',
    link3: 'Search Salary',
    link4: 'Ask for referrals',
    link5: 'Recommend',
    link6: 'Refer this extension',
}

export const navigationContants = {
    dashboard: '/',
    applieadJobs: '/applied-jobs',
    searchJobs: '/search-jobs',
    searchSalary: '/search-salary',
    referral: '/referral',
    recommendation: '/recommendation',
    shareExtension: '/share-extension',
  };
  


export const iconConstants = {
    weekdayLogo: 'weekdayLogo',
    weekdayLogoIcon: 'weekdayLogoIcon',
    hourGlass: 'hourGlass',
    lightningIcon: 'lightningIcon',
    nothingFound: 'nothingFound',

    appliedJobs: 'appliedJobs',
    searchJobs: 'searchJobs',
    searchSalary: 'searchSalary',
    referral: 'referral',
    share: 'share',
    recommend: 'recommend',

    leftArrow: 'leftArrow',
    rightArrow: 'rightArrow',



    
    productIcon: 'productIcon',
    fetchProductIcon: 'fetchProductIcon',
    userProfileIcon: 'userProfileIcon',
    productJourneyIcon: 'productJourneyIcon',
    helpNSupportIcon: 'helpNSupportIcon',
  
  
    productIconFilled: 'productIconFilled',
    fetchProductIconFilled: 'fetchProductIconFilled',
    userProfileIconFilled: 'userProfileIconFilled',
    productJourneyIconFilled: 'productJourneyIconFilled',
    helpNSupportIconFilled: 'helpNSupportIconFilled',



  //pngs
  errorVector: 'errorVector',
  logo: 'logo',

  billHeadPng: 'billHeadPng',
  billFootPng: 'billFootPng',
  closeCirclePng: 'closeCirclePng',
  

  //svgs
  noQueries: 'noQueries',
  arrowDownBlack: 'arrowDownBlack',
  roundedLeftArrowBlack: 'roundedLeftArrowBlack',
  roundedLeftArrowGray: 'roundedLeftArrowGray',
  roundedRightArrowBlack: 'roundedRightArrowBlack',
  roundedRightArrowGray: 'roundedRightArrowGray',

  grayLeftArrowDisabled: 'grayLeftArrowDisabled',
  grayLeftArrowEnabled: 'grayLeftArrowEnabled',
  grayRightArrowDisabled: 'grayRightArrowDisabled',
  grayRightArrowEnabled: 'grayRightArrowEnabled',

  billHeaderDesign: 'billHeaderDesign',
  billFooterDesign: 'billFooterDesign',
  billHeaderDesign1: 'billHeaderDesign1',

  pause: 'pause',
  resume: 'resume',
  duplicate: 'duplicate',
  addPromotion: 'addPromotion',

  profileIcon: 'profileIcon',
  noBills: 'noBills',
  retailerPlaceholder: 'retailerPlaceholder',
  drawerControllerLeft: 'drawerControllerLeft',
  drawerControllerRight: 'drawerControllerRight',
  alertCircle: 'alertCircle',
  alertCircleBlack: 'alertCircleBlack',
  alertCircleFilled: 'alertCircleFilled',
  tickCircle: 'tickCircle',

  visibilityIcon: 'visibilityIcon',
  visibilityOff: 'visibilityOff',

  messageQuestion: 'messageQuestion',
  profileUser: 'profileUser',
  receiptItem: 'receiptItem',
  stickyNote: 'stickyNote',
  voucher: 'voucher',

  messageQuestionFilled: 'messageQuestionFilled',
  profileUserFilled: 'profileUserFilled',
  receiptItemFilled: 'receiptItemFilled',
  stickyNoteFilled: 'stickyNoteFilled',
  voucherFilled: 'voucherFilled',

  arrowDown: 'arrowDown',
  upArrow: 'upArrow',
  downArrow: 'downArrow',
  salesUp: 'salesUp',
  salesDown: 'salesDown',
  noProductJourny: 'noProductJourny',


  manufactured: 'manufactured',
  retailed: 'retailed',
  distributed: 'distributed',
  sold: 'sold',



  cancelButton: 'cancelButton',
  addWhite: 'addWhite',
  filter: 'filter',
  searchNormal: 'searchNormal',
  ascArrow: 'ascArrow',
  descArrow: 'descArrow',
  resendInvitation: 'resendInvitation',
  trash: 'trash',
  upload: 'upload',
  upload2: 'upload2',
  checkSquare: 'checkSquare',
  uncheckSquare: 'uncheckSquare',
  alertOctagon: 'alertOctagon',
  radioButtonUnchecked: 'radioButtonUnchecked',
  radioButtonChecked: 'radioButtonChecked',
  editIcon: 'editIcon',
  closeSquare: 'closeSquare',
  profileEditIcon: 'profileEditIcon',
  closeCircle: 'closeCircle',
  toastError: 'toastError',
  toastSuccess: 'toastSuccess',
  minus: 'minus',
  noReceipts: 'noReceipts',
  caretLeft: 'caretLeft',
  download: 'download',

  //admin portal

  //svgs
  licensesLogo: 'licensesLogo',
  add: 'add',
  plus: 'plus',
  alertCircleWhite: 'alertCircleWhite',

  editRetailerProfile: 'editRetailerProfile',
  imagePlaceholder: 'imagePlaceholder',
  product: 'product',
  productCatalogueLogo: 'productCatalogueLogo',
  promotions: 'promotions',
  retailers: 'retailers',
  productCategoriesGreen: 'productCategoriesGreen',
  promotionsGreen: 'promotionsGreen',
  retailersGreen: 'retailersGreen',
  users: 'users',
  caretRight: 'caretRight',
  usersGreen: 'usersGreen',

  noMacIds: 'noMacIds',
  caretDown: 'caretDown',
  arrowRight: 'arrowRight',
  retailersNameLogo: 'retailersNameLogo',
  closeIcon: 'closeIcon',
  menuDots: 'menuDots',
  trashBlack: 'trashBlack',
  refresh: 'refresh',
  xlsxIcon: 'xlsxIcon',
  calendar: 'calendar',
  editIcon25px: 'editIcon25px',
  signOut: 'signOut',
  arrowLeftCircle: 'arrowLeftCircle',

  manualExpenseFilled: 'manualExpenseFilled',
  manualExpenseBlack: 'manualExpenseBlack',

  //pngs
  actAuthImg: 'actAuthImg',
  authExpiredLogo: 'authExpiredLogo',
  authLogo: 'authLogo',
  profileLogo: 'profileLogo',
  retailerInfoLogo: 'retailerInfoLogo',
  retailersLogo: 'retailersLogo',
  sidebarScrollLeft: 'sidebarScrollLeft',
  sidebarScrollRight: 'sidebarScrollRight',
  signIn: 'signIn',
  vouchers: 'vouchers',
  profilePic: 'profilePic',
  addPromotions: 'addPromotions',
  noPromotion: 'noPromotion',
  activatingUserLogo: 'activatingUserLogo',
  voucherTemplate: 'voucherTemplate',

  caretDown: 'caretDown',

  arrowRight: 'arrowRight',
  retailersNameLogo: 'retailersNameLogo',
  closeIcon: 'closeIcon',
  menuDots: 'menuDots',
  lock: 'lock',
  noData: 'noData',
  promotionsPlaceholder: 'promotionsPlaceholder',
  appLoader: 'appLoader',

  //pngs
  actAuthImg: 'actAuthImg',
  authExpiredLogo: 'authExpiredLogo',
  authLogo: 'authLogo',
  profileLogo: 'profileLogo',
  retailerInfoLogo: 'retailerInfoLogo',
  retailersLogo: 'retailersLogo',
  sidebarScrollLeft: 'sidebarScrollLeft',
  sidebarScrollRight: 'sidebarScrollRight',
  signIn: 'signIn',
  vouchers: 'vouchers',
  profilePic: 'profilePic',
  addPromotions: 'addPromotions',
  activatingUserLogo: 'activatingUserLogo',
  voucherTemplate: 'voucherTemplate',

  pageNotFound: 'pageNotFound',

  //vouchers
  voucherImagePlaceholder: 'voucherImagePlaceholder',
  vouchersTableImagePlaceholder: 'vouchersTableImagePlaceholder',
  userPlaceholder: 'userPlaceholder',
  copyToClip: 'copyToClip',
  downloadKey: 'downloadKey',

  sortArrow: 'sortArrow',
  pending: 'pending',
  resolved: 'resolved',
  sendEnable: 'sendEnable',
  sendDisable: 'sendDisable',
  arrowSend: 'arrowSend',
  arrowReceive: 'arrowReceive',

  notificationBing: 'notificationBing',
  privateVaultBlack: 'privateVaultBlack',
  receipt: 'receipt',
  tickCircleGreen: 'tickCircleGreen',
  alertCircleBlackNew: 'alertCircleBlackNew',
  checks: 'checks',
};


export const addItemsTableSelectFieldSX = {
    flex: 1,
    '& .MuiOutlinedInput-notchedOutline': {
      borderColor: 'transparent',
    },
    '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      border: '1px solid',
      borderColor: 'inputFieldColors.border',
    },
    '&:hover .MuiOutlinedInput-notchedOutline': {
      border: '1px solid',
      borderColor: 'inputFieldColors.border',
    },
  };
  


export const addItemsTableTextFieldSX = {
    flex: 1,
    '&.MuiOutlinedInput-root': {
      fontFamily: (theme) => theme.typography.body3.fontFamily,
      fontSize: (theme) => theme.typography.body3.fontSize,
      lineHeight: (theme) => theme.typography.body3.lineHeight,
      fontWeight: (theme) => theme.typography.body3.fontWeight,
      letterSpacing: (theme) => theme.typography.body3.letterSpacing,
      color: (theme) => theme.typography.body3.color,
      '& > fieldset': {
        borderColor: 'transparent',
      },
      '&:hover fieldset': {
        borderColor: 'inputFieldColors.borderFocus',
      },
      '&.Mui-focused fieldset': {
        border: '1px solid',
        borderColor: 'inputFieldColors.borderFocus',
      },
    },
  };
  