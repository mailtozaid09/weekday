import React, { lazy, Suspense, useEffect, useState } from 'react';
import { CssBaseline, ThemeProvider, } from '@mui/material';

import { createTheme } from '@mui/material/styles'



import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import Loading from '../src/components/Loading';
import { ToastProvider } from '../src/components/ToastContext';

import { dark, light } from '../src/resources/theme/themes';
import { typography } from './resources/theme/typography';

const DashboardPage = lazy(() => import('../src/pages/dashboard'));
const Dashboard = lazy(() => import('../src/pages/dashboard/Dashboard'));

const AppliedJobs = lazy(() => import('../src/pages/appliedJobs/AppliedJobs'));
const SearchJobs = lazy(() => import('../src/pages/searchJobs/SearchJobs'));
const SearchSalary = lazy(() => import('../src/pages/searchSalary/SearchSalary'));
const Referral = lazy(() => import('../src/pages/referrals/Referrals'));

const Recommendation = lazy(() => import('../src/pages/recommendation/Recommendation'));
const ShareExtension = lazy(() => import('../src/pages/shareExtension/ShareExtension'));



const App = () => {
    const [lightMode, setLightMode] = useState(true);
    const [typo, setTypo] = useState(typography);
    const appliedTheme = createTheme(
        lightMode ? { ...light, ...typo } : { ...dark, ...typo }
    );

    return (
        <ThemeProvider theme={appliedTheme}>
        <ToastProvider>
                <CssBaseline enableColorScheme />
                <Suspense fallback={<Loading />}>
                    <BrowserRouter>
                    <Routes>
                        <Route path='/'>
                            <Route
                                element={<Dashboard />}
                                >
                                    <Route path='dashboard' exact element={<DashboardPage />} />
                                    <Route index element={<Navigate to='/applied-jobs' replace />} />
                                    

                                    <Route path='/applied-jobs' element={<AppliedJobs />} />
                                    <Route path='/search-jobs' element={<SearchJobs />} />
                                    <Route path='/search-salary' element={<SearchSalary />} />
                                    <Route path='/referral' element={<Referral />} />

                                    <Route path='/recommendation' element={<Recommendation />} />
                                    <Route path='/share-extension' element={<ShareExtension />} />
                            </Route>

                            {/* <Route path='sign-in' element={<SignIn />} />

                            <Route path='activation' element={<Activation />} />
                            
                            <Route path='forgot-password' element={<ForgotPassword />} />
                            <Route path='reset-password' element={<ResetPassword />} />
                        
                            <Route path='not-found' element={<PageNotFound />} />
                            <Route path='*' element={<PageNotFound />} /> */}
                        </Route>
                    </Routes>
                    </BrowserRouter>
                </Suspense>
            </ToastProvider>
        </ThemeProvider>
    )
}

export default App