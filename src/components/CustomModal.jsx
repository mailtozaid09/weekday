import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { typographyConstants } from '../resources/theme/constants';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxHeight: '80%', 
    overflowY: 'auto', 
    borderRadius: '10px',
    bgcolor: 'white',
    border: '1px solid #000',
    p: 4,
};

const backdropStyle = {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
};

export default function CustomModal({ showModal, handleModalClose, aboutCompany, aboutRole, companySkills, companyQualifications, companyResponsibilities }) {

    var aboutCompanyDetails = aboutCompany || "Weekday, We help companies hire by building a highly curated list of engineering candidates for them; crowdsourced by our network of software engineers who moonlight as “mini-recruiters” or scouts. Our product enables anyone to become that scout and earn passive income."
    var aboutRoleDetails = aboutRole || "We are seeking a skilled and experienced Mobile Application Developer proficient in technologies such as React Native and iOS. As a Mobile Application Developer, you will be responsible for designing, developing, and maintaining mobile applications for our company. The ideal candidate should have a minimum of 1 year of relevant experience in mobile application development and possess a strong understanding of React Native and iOS development. You will collaborate with cross-functional teams to create high-quality, user-friendly, and efficient mobile applications."
    var companySkillsDetails = companySkills || "react native,reactnative,react native developer,ios,ios app development,mobile app development,mobile applications,mobile ios application"
    var companyQualificationsDetails = companyQualifications || "Minimum of 1 year of professional experience in mobile application development.Proficiency in React Native and iOS development.Strong knowledge of JavaScript, TypeScript, HTML, and CSS.Experience with mobile application development frameworks and tools.Familiarity with RESTful APIs and integration of mobile applications with backend services.Understanding of mobile application architecture and design patterns.Knowledge of version control systems, such as Git.Experience with unit testing and debugging mobile applications.Strong problem-solving and analytical skills.Excellent communication and collaboration skills.Ability to work in a fast-paced and dynamic environment.Bachelor's degree in Computer Science, Software Engineering, or a related field (preferred)"
    var companyResponsibilitiesDetails = companyResponsibilities || "Develop high-quality mobile applications using React Native and iOS technologies.Collaborate with designers, product managers, and other stakeholders to understand project requirements and translate them into technical specifications.Participate in the entire application development lifecycle, including concept, design, build, deploy, test, and release.Conduct thorough code reviews to ensure code quality, maintainability, and adherence to best practices.Debug and resolve issues, bugs, and performance bottlenecks in existing mobile applications.Stay up-to-date with the latest trends and advancements in mobile application development technologies and frameworks.Optimize application performance and improve user experience.Collaborate with backend developers to integrate mobile applications with server-side systems and APIs.Write clean, reusable, and maintainable code following industry standards and best practices.Document technical specifications, project progress, and other relevant information.Assist in the planning and estimation of mobile application development projects.Continuously learn and improve skills in mobile application development and related"
    
    const [screenWidth, setScreenWidth] = useState(400);

    useEffect(() => {
        updateScreenWidth();
      }, [screenWidth]);

    
    useEffect(() => {
        window.addEventListener('resize', updateScreenWidth);
        return () => window.removeEventListener('resize', updateScreenWidth);
    });
    
      const updateScreenWidth = () => {
     
        if(window.innerWidth < 420){
            setScreenWidth(300)
        }else if(window.innerWidth < 560){
            setScreenWidth(400)
        }else if(window.innerWidth < 786){
            setScreenWidth(500)
        }else{
            setScreenWidth(650)
        }
        
      };
      
    return (
        <div>
            <Modal
                open={showModal}
                onClose={handleModalClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                BackdropProps={{ style: backdropStyle }}
                
            >
                <Box sx={[style, {width: screenWidth}]}>
                    <Typography variant={typographyConstants.callout1} sx={{alignItems: 'center', justifyContent: 'center', display: 'flex'}} >
                        Job Description
                    </Typography>

                    <Box sx={{display: 'flex', flexDirection: 'column'}} >
                        <Typography variant={typographyConstants.callout3} sx={{mt: '30px'}} >
                            About Company:
                        </Typography>

                        <Typography variant={typographyConstants.callout2} sx={{}} >
                            {aboutCompanyDetails}
                        </Typography>


                        <Typography variant={typographyConstants.callout3} sx={{mt: '10px'}} >
                            About Role:
                        </Typography>

                        <Typography variant={typographyConstants.callout2} sx={{}} >
                            {aboutRoleDetails}
                        </Typography>


                        <Typography variant={typographyConstants.callout2} sx={{mt: '20px', mb: '20px'}} >
                            Responsibility:
                        </Typography>
                        <Typography variant={typographyConstants.callout2} sx={{}} >
                            {companyResponsibilitiesDetails}
                        </Typography>


                        <Typography variant={typographyConstants.callout2} sx={{mt: '20px', mb: '20px'}} >
                            Qualifications:
                        </Typography>
                        <Typography variant={typographyConstants.callout2} sx={{}} >
                            {companyQualificationsDetails}
                        </Typography>

                        <Typography variant={typographyConstants.callout2} sx={{mt: '20px', mb: '20px'}} >
                            Skills:
                        </Typography>
                        <Typography variant={typographyConstants.callout2} sx={{}} >
                            {companySkillsDetails}
                        </Typography>


                    </Box>
                </Box>
            </Modal>
        </div>
    );
}
