import React from 'react';
import { Box, Backdrop } from '@mui/material';
import { iconConstants } from '../resources/theme/constants';
import CustomIcon from './CustomIcon';
const Loading = () => {
  return (
    <Box>
      <Backdrop
        style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)', zIndex: 9999 }}
        classes={{
          root: 'MuiBackdrop-root-loader',
        }}
        open={true}
      >
        <CustomIcon name={iconConstants.appLoader} />
      </Backdrop>
    </Box>
  );
};

export default Loading;
