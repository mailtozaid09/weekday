import * as React from 'react';
import { Box,  Checkbox, ThemeProvider, Typography } from '@mui/material';
import CustomIcon from './CustomIcon';
import { iconConstants, typographyConstants } from '../resources/theme/constants';
import CustomModal from './CustomModal';
import { capitalizeSentence, getRandomNumber, getSalaryRange, isPrimeNumber } from '../utils/utils';

export default function JobsCard({index, data, sx_style}) {

    const { 
        jdUid,
        jdLink,
        jobDetailsFromCompany,
        maxJdSalary,
        minJdSalary,
        salaryCurrencyCode,
        location,
        minExp,
        maxExp,
        jobRole,
        companyName,
        logoUrl
    } = data



    const [showMore, setShowMore] = React.useState(false);

    const [showModal, setShowModal] = React.useState(false);
    const handleModalOpen = () => setShowModal(true);
    const handleModalClose = () => setShowModal(false);

    return (
        <ThemeProvider
            theme={{
                palette: {
                primary: {
                    main: '#ffffff',
                    dark: '#ffffff80',
                },
                },
            }}
            >
            <Box
                sx={[
                    {
                        display: 'flex',
                        flexDirection: 'column',
                        borderRadius: '20px',
                        p: '14px',
                        bgcolor: 'primary.main',
                        boxShadow: 'rgba(0, 0, 0, 0.25) 0px 1px 4px 0px',
                        '&:hover': {
                            bgcolor: 'primary.dark',
                            transform: 'scale(1.01)',
                        },
                    },
                    sx_style
                ]}
            >
                
                    <Box sx={{ width: 'fit-content', mb: '14px', p: '4px', px: '10px', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', display: 'flex', borderRadius: '10px', boxShadow: 'rgba(6, 6, 6, 0.05) 0px 2px 6px 0px'}} >
                        <CustomIcon
                        name={iconConstants.hourGlass}
                        style={{
                            height: '14px',
                            width: '14px',
                        }}
                        />  
                        <Typography sx={{fontSize: '10px', ml: '4px' }} >
                        Posted {getRandomNumber()} days ago
                        </Typography>
                    </Box>

                    <Box sx={{ mb: '4px', flexDirection: 'row',  display: 'flex' }} >
                        <img
                        src={logoUrl}
                        style={{
                            height: '40px',
                            width: '40px',
                            borderRadius: '8px',
                        }}
                        />  
                        <Box sx={{ml: '10px',}} >
                            <Typography 
                                onClick={() => {window.open(jdLink, '_blank')}}
                                sx={{ fontSize: '14px', fontWeight: '600', color: '#8b8b8b', '&:hover': {cursor: 'pointer', textDecorationThickness: '2px', textDecorationLine: 'underline' }, }} >
                                {companyName}
                            </Typography>
                            <Typography sx={{fontSize: '14px',  }} >
                            {capitalizeSentence(jobRole)}
                            </Typography>
                            <Typography sx={{fontSize: '12px', }} >
                            {capitalizeSentence(location)}
                            </Typography>
                        </Box>
                    </Box>

                    <Box>
                        <Typography variant={typographyConstants.callout2}>
                            Estimated Salary: {getSalaryRange(minJdSalary, maxJdSalary, salaryCurrencyCode)}
                        </Typography>
                    </Box>

                    <Box sx={{mb: '10px', flexDirection: 'column', display: 'flex'}} >
                        <Typography variant={typographyConstants.callout3}>
                            About Company:
                        </Typography>

                        <Typography variant={typographyConstants.subtitle1 }>
                            About us
                        </Typography>


                        <Box>
                            <div
                            style={{
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                display: '-webkit-box',
                                WebkitLineClamp: showMore ? 'unset' : 7,
                                WebkitBoxOrient: 'vertical',
                                lineHeight: '1.5em',
                            }}
                            >
                                <Typography variant="subtitle1">
                                    {jobDetailsFromCompany}
                                </Typography>
                            </div>

                    
                            <Box sx={{mt: '4px', alignItems: 'center', justifyContent: 'center', display: 'flex', }} >
                                {!showMore && (
                                    <Box onClick={handleModalOpen} color="primary" sx={{ '&:hover': {cursor: 'pointer', }, }}>
                                        <Typography variant={typographyConstants.subtitle1} sx={{color: '#4943da', }} >
                                            Show more
                                        </Typography>
                                    </Box>
                                )}
                            </Box>
                            
                        </Box>
                    </Box>


                    <Box sx={{mb: '10px', flexDirection: 'column', display: 'flex'}} >
                        <Typography variant={typographyConstants.callout2}>
                            Minimum Experience
                        </Typography>

                        <Typography variant={typographyConstants.callout2 }>
                            {minExp || '2'} years
                        </Typography>
                    </Box>

                    
                    <Box sx={{display: 'flex', height: '100%', flexDirection: 'column', justifyContent: 'flex-end'}} >

                        <Box 
                            onClick={() => {window.open(jdLink, '_blank')}}
                            sx={{height: '50px', width: '100%', alignItems: 'center', justifyContent: 'center', display: 'flex', borderRadius: '8px', background: '#55efc4',  '&:hover': {cursor: 'pointer', transform: 'scale(1.01)'},}} >
                            <CustomIcon
                                name={iconConstants.lightningIcon}
                                style={{
                                    height: '24px',
                                    width: '24px',
                                }}
                            />  
                            <Typography variant={typographyConstants.callout2} sx={{ml: '4px'}} >
                                Easy Apply
                            </Typography>
                        </Box>

                        {isPrimeNumber(index) && (
                        <Box 
                            onClick={() => {window.open(jdLink, '_blank')}}
                            sx={{mt: '10px', height: '50px', width: '100%', alignItems: 'center', justifyContent: 'center', display: 'flex', borderRadius: '8px', background: '#4943da',  '&:hover': {cursor: 'pointer', transform: 'scale(1.01)'},}} >
                            <CustomIcon
                                name={iconConstants.userPlaceholder}
                                svgStyle={'height: 30px; width: 30px; border-radius: 20px; margin-top: 4px'}
                            />  
                            <Typography variant={typographyConstants.callout2} sx={{ml: '4px', color: 'white'}} >
                                Ask for referral
                            </Typography>
                        </Box>)}
                    </Box>
                            

                    <div>

                <CustomModal
                    showModal={showModal}
                    handleModalOpen={handleModalOpen}
                    handleModalClose={handleModalClose}
                    aboutCompany={jobDetailsFromCompany}
                />
            </div>
            </Box>
        </ThemeProvider>
    );
}
