import React, { useState } from 'react';
import { Box, Select, MenuItem, ListItemText, Checkbox, Chip, InputLabel, Typography } from '@mui/material';
import CustomIcon from './CustomIcon'; 
import { iconConstants, addItemsTableSelectFieldSX, typographyConstants } from '../resources/theme/constants';

import useStyles from './styles';

const CustomMultiSelect = ({ dataArray, title, minWidth = 220, selectedValue, handleChange, handleDelete }) => {
    const classes = useStyles();
    const [data, setData] = useState(dataArray);

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: '226px',
            },
        },
    };

    return (
        <Box sx={{display: 'flex', flexDirection: 'column', width: 'fit-content'}} >
            <Typography variant={typographyConstants.caption} >
                {title}
            </Typography>
            <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                className={classes.textField}
                multiple
                size='small'
                autoFocus={false}
                value={selectedValue}
                onChange={handleChange}
                sx={{
                    ...addItemsTableSelectFieldSX,
                    minWidth: minWidth,
                    flexWrap: 'wrap',
                    border: '0.5px solid black'
                }}
                renderValue={(selected) => (
                    <Box
                        sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            gap: 0.5,
                        }}
                    >
                        {selected.map((value, index) => (
                            <Box sx={{display: 'flex', flexDirection: 'row'}} >
                                <Chip
                                key={value}
                                sx={{
                                    backgroundColor: 'globalElementColors.canvas2',
                                    height: '26px',
                                    borderRadius: '5px',
                                    padding: '2px 5px',
                                }}
                                label={value}
                                clickable
                                deleteIcon={
                                    <CustomIcon
                                        name={iconConstants.closeSquare}
                                        onMouseDown={(e) => {
                                            e.stopPropagation();
                                            handleDelete(value)
                                        }}
                                    />
                                }
                                onDelete={(e) => {
                                    e.stopPropagation();
                                }}
                            />
                            
                            </Box>
                        ))}
                          <Chip
                                key={'Clear All'}
                                sx={{
                                    backgroundColor: 'globalElementColors.canvas2',
                                    height: '26px',
                                    borderRadius: '5px',
                                    padding: '2px 5px',
                                }}
                                label={'Clear All'}
                                clickable
                                deleteIcon={
                                    <CustomIcon
                                        name={iconConstants.closeSquare}
                                        onMouseDown={(e) => {
                                            e.stopPropagation();
                                            handleDelete('clear')
                                        }}
                                    />
                                }
                                onDelete={(e) => {
                                    e.stopPropagation();
                                }}
                            />
                    </Box>
                )}
                MenuProps={MenuProps}
            >
                {data.map((value) => (
                    <MenuItem
                        className={classes.textField}
                        key={value}
                        value={value}
                        sx={{
                            '&.Mui-selected': {
                                backgroundColor: (theme) =>
                                    theme.palette.globalElementColors.white,
                            },
                            '&.Mui-selected:hover': {
                                backgroundColor: (theme) =>
                                    theme.palette.globalElementColors.canvas1,
                            },
                        }}
                    >
                        <Checkbox
                            checked={selectedValue ? selectedValue.indexOf(value) > -1 : false}
                            icon={
                                <CustomIcon
                                    name={iconConstants.uncheckSquare}
                                    style={{ height: '21px', width: '21px',  }}
                                />
                            }
                            checkedIcon={
                                <CustomIcon
                                    name={iconConstants.checkSquare}
                                    style={{ height: '21px', width: '21px' }}
                                />
                            }
                        />
                        <ListItemText primary={value} />
                    </MenuItem>
                ))}
            </Select>
        </Box>
    );
};

export default CustomMultiSelect;
