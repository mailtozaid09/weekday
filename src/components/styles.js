import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
//   pageCenteredBox: {
//     margin: 0,
//     position: 'absolute',
//     top: '50%',
//     left: '50%',
//     transform: 'translate(-50%, -50%)',
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'center',
//     alignItems: 'center',
//     alignContent: 'center',
//   },

//   flexCenter: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },

//   flexAlignCenter: {
//     display: 'flex',
//     alignItems: 'center',
//   },

//   flexJustifySpaceBetween: {
//     display: 'flex',
//     justifyContent: 'space-between',
//   },

//   flexcenterJustifyFlexStart: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'flex-start',
//   },

//   flexAlignStartJustifycenter: {
//     display: 'flex',
//     alignItems: 'flex-start',
//     justifyContent: 'center',
//   },

//   flexAlignStartJustifystart: {
//     display: 'flex',
//     alignItems: 'flex-start',
//     justifyContent: 'flex-start',
//   },

//   flexRowCenter: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },

//   flexRowRight: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//   },

//   flexRowLeft: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'flex-start',
//   },

//   flexRowLeftCenter: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'flex-start',
//     alignItems: 'center',
//   },

//   flexRowSpaceBetween: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//   },

//   flexRowAlignStartJustifySpaceBetween: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'flex-start',
//   },

//   flexRowSpaceBetweenNoCenter: {
//     display: 'flex',
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//   },

//   flexRowFlexEnd: {
//     display: 'flex',
//     justifyContent: 'flex-end',
//   },

//   flexColumnCenter: {
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },

//   flexColumnCenterStart: {
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'center',
//     alignItems: 'flex-start',
//   },

//   flexColumnLeft: {
//     display: 'flex',
//     alignItems: 'flex-start',
//     flexDirection: 'column',
//   },

//   flexColumnEnd: {
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//   },

//   flexColumnSpaceBetween: {
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'space-between',
//   },

//   flexColumnJustifyStart: {
//     display: 'flex',
//     flexDirection: 'column',
//     justifyContent: 'start',
//     alignItems: ' center',
//   },

//   tableRowFlexLeftJustifycontent: {
//     display: 'flex',
//     alignItems: 'center',
//     padding: '12px 10px',
//     height: '60px',
//   },

//   tableCellPaddingZero: {
//     paddingLeft: '0px',
//     paddingRight: '0px',
//   },

//   buttonHeightWidthUpload: {
//     width: '135px !important',
//     height: '35px',
//   },

//   tableRowStyles: {
//     borderRadius: '20px',
//     borderTop: '10px solid' ,
//     //+ theme.palette.tableViewRow.main,
//     padding: '20px',
//     minHeight: '60px',
//     '&:last-child td, &:last-child th': {
//       border: 0,
//     },
//   },

//   menuSliderContainer: {
//     // width: 400,
//     height: '100%',
//   },

//   customButtonClass: {
//     padding: '16px 20px',
//     height: '35px',
//     width: '135px',
//     borderRadius: '6px',
//   },

//   buttonColors: {
//     backgroundColor: theme.palette.buttonColors.main,
//   },

//   filterButtonText: {
//     fontSize: 'var(--font16)',
//     fontWeight: 'var(--fontWeight500)',
//   },

//   validateTextStyle: {
//     width: '18px',
//     height: '16px',
//     marginRight: '7px',
//   },

//   borderZero: {
//     border: '0px',
//   },

//   textField: {
//     '&.MuiInputBase-input, .MuiChip-label, .MuiListItemText-primary': {
//       fontFamily: theme.typography.body3.fontFamily,
//       fontSize: theme.typography.body3.fontSize,
//       lineHeight: theme.typography.body3.lineHeight,
//       fontWeight: theme.typography.body3.fontWeight,
//       letterSpacing: theme.typography.body3.letterSpacing,
//       color: theme.typography.body3.color,
//     },
//   },

//   buttonBgFontColor: {
//     backgroundColor: theme.palette.buttonColors.main + ' !important',
//     color: theme.palette.buttonColors.contrastText + ' !important',
//   },

//   paddingTransition: {
//     transition: 'left 0.28s ease-in-out',
//   },

//   listDownTransition: {
//     transition: 'height 1s ease-in-out',
//   },

//   toast: {
//     position: 'fixed',
//     bottom: '20px',
//     right: '20px',
//     color: '#fff',
//     padding: '12px',
//     borderRadius: '4px',
//     transition: 'opacity 0.5s ease-in-out',
//     zIndex: theme.zIndex.drawer + 1,
//   },

//   tableRow: {
//     '& .MuiTableRow-root': {
//       height: 'auto',
//     },
//     '& .MuiTableCell-root': {
//       padding: 0,
//       borderBottom: 0,
//     },
//   },
}));

export default useStyles;
