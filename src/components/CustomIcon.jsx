import React from 'react';
import { ReactSVG } from 'react-svg';

import toastError from '../resources/images/svg/toastError.svg';
import toastSuccess from '../resources/images/svg/toastSuccess.svg';
import closeCircle from '../resources/images/svg/closeCircle.svg';

import drawerControllerLeft from '../resources/images/svg/drawerControllerLeft.svg';
import drawerControllerRight from '../resources/images/svg/drawerControllerRight.svg';

import appLoader from '../resources/images/svg/appLoader.svg';
import signOut from '../resources/images/svg/signOut.svg';
import userPlaceholder from '../resources/images/svg/userPlaceholder.svg';

import editIcon from '../resources/images/svg/editIcon.svg';
import editIcon25px from '../resources/images/svg/editIcon25px.svg';
import upload from '../resources/images/svg/upload3.svg';
import upload2 from '../resources/images/svg/upload2.svg';

import closeSquare from '../resources/images/svg/closeSquare.svg';

import trash from '../resources/images/svg/trashRed.svg';
import trashBlack from '../resources/images/svg/trashBlack.svg';


import alertOctagon from '../resources/images/svg/alertOctagon.svg';
import radioButtonUnchecked from '../resources/images/svg/radioButtonUnchecked.svg';
import radioButtonChecked from '../resources/images/svg/radioButtonChecked.svg';

import addWhite from '../resources/images/svg/addWhite.svg';

import descArrow from '../resources/images/svg/descArrow.svg';
import ascArrow from '../resources/images/svg/ascArrow.svg';

import filter from '../resources/images/svg/filter.svg';
import searchNormal from '../resources/images/svg/searchNormal.svg';

import notificationBing from '../resources/images/svg/notification-bing.svg';

import grayLeftArrowDisabled from '../resources/images/svg/grayLeftArrowDisabled.svg';
import grayLeftArrowEnabled from '../resources/images/svg/grayLeftArrowEnabled.svg';
import grayRightArrowDisabled from '../resources/images/svg/grayRightArrowDisabled.svg';
import grayRightArrowEnabled from '../resources/images/svg/grayRightArrowEnabled.svg';

import arrowDownBlack from '../resources/images/svg/arrowDownBlack.svg';
import roundedLeftArrowBlack from '../resources/images/svg/roundedLeftArrowBlack.svg';
import roundedLeftArrowGray from '../resources/images/svg/roundedLeftArrowGray.svg';
import roundedRightArrowBlack from '../resources/images/svg/roundedRightArrowBlack.svg';
import roundedRightArrowGray from '../resources/images/svg/roundedRightArrowGray.svg';

import profileUser from '../resources/images/svg/profileUser.svg';
import receiptItem from '../resources/images/svg/receiptItem.svg';
import stickyNote from '../resources/images/svg/stickyNote.svg';
import voucher from '../resources/images/svg/voucher.svg';
import messageQuestion from '../resources/images/svg/messageQuestion.svg';

import profileUserFilled from '../resources/images/svg/profileUserFilled.svg';
import receiptItemFilled from '../resources/images/svg/receiptItemFilled.svg';
import stickyNoteFilled from '../resources/images/svg/stickyNoteFilled.svg';
import voucherFilled from '../resources/images/svg/voucherFilled.svg';
import messageQuestionFilled from '../resources/images/svg/messageQuestionFilled.svg';


import appliedJobs from '../resources/images/svg_image/jobs.svg';
import searchJobs from '../resources/images/svg_image/search.svg';
import searchSalary from '../resources/images/svg_image/salary.svg';
import referral from '../resources/images/svg_image/referral.svg';
import share from '../resources/images/svg_image/share.svg';
import recommend from '../resources/images/svg_image/recommend.svg';
import left_arrow from '../resources/images/svg_image/left_arrow.svg';
import right_arrow from '../resources/images/svg_image/right_arrow.svg';

import checkSquare from '../resources/images/svg_image/checkSquare.svg';
import uncheckSquare from '../resources/images/svg_image/uncheckSquare.svg';
import nothingFound from '../resources/images/svg_image/notfound.svg';



//-----------PNG's----------------
import closeCirclePng from '../resources/images/png/closeCirclePng.png';
import weekdayLogo from '../resources/images/png/weekday_logo.png';
import weekdayLogoIcon from '../resources/images/png/weekday_logo_icon.png';

import hourGlass from '../resources/images/png/hourglass.png';
import lightningIcon from '../resources/images/png/light.png';



const CustomIcon = ({
  name,
  style,
  className,
  onClick,
  alt,
  onMouseDown,
  svgStyle,
}) => {


  const customer_icon = {
    closeCirclePng: (
        <img
          src={closeCirclePng}
          style={style}
          className={className}
          alt={alt}
          onClick={onClick}
        />
      ),
      weekdayLogo: (
        <img
          src={weekdayLogo}
          style={style}
          className={className}
          alt={alt}
          onClick={onClick}
        />
      ),
      weekdayLogoIcon: (
        <img
          src={weekdayLogoIcon}
          style={style}
          className={className}
          alt={alt}
          onClick={onClick}
        />
      ),
      hourGlass: (
        <img
          src={hourGlass}
          style={style}
          className={className}
          alt={alt}
          onClick={onClick}
        />
      ),
      lightningIcon: (
        <img
          src={lightningIcon}
          style={style}
          className={className}
          alt={alt}
          onClick={onClick}
        />
      ),
      nothingFound: (
        <ReactSVG src={nothingFound} style={style} className={className} />
      ),
      appliedJobs: (
        <ReactSVG src={appliedJobs} style={style} className={className} />
      ),
      searchJobs: (
        <ReactSVG src={searchJobs} style={style} className={className} />
      ),
      searchSalary: (
        <ReactSVG src={searchSalary} style={style} className={className} />
      ),
      referral: (
        <ReactSVG src={referral} style={style} className={className} />
      ),
      share: (
        <ReactSVG src={share} style={style} className={className} />
      ),
      recommend: (
        <ReactSVG src={recommend} style={style} className={className} />
      ),
      leftArrow: (
        <ReactSVG src={left_arrow} style={style} className={className} />
      ),
      rightArrow: (
        <ReactSVG src={right_arrow} style={style} className={className} />
      ),
      profileUser: (
        <ReactSVG src={profileUser} style={style} className={className} />
      ),
      receiptItem: (
        <ReactSVG src={receiptItem} style={style} className={className} />
      ),
      stickyNote: (
        <ReactSVG src={stickyNote} style={style} className={className} />
      ),
      voucher: (
        <ReactSVG src={voucher} style={style} className={className} />
      ),
      messageQuestion: (
        <ReactSVG src={messageQuestion} style={style} className={className} />
      ),
  
      profileUserFilled: (
        <ReactSVG src={profileUserFilled} style={style} className={className} />
      ),
      receiptItemFilled: (
        <ReactSVG src={receiptItemFilled} style={style} className={className} />
      ),
      stickyNoteFilled: (
        <ReactSVG src={stickyNoteFilled} style={style} className={className} />
      ),
      voucherFilled: (
        <ReactSVG src={voucherFilled} style={style} className={className} />
      ),
      messageQuestionFilled: (
        <ReactSVG src={messageQuestionFilled} style={style} className={className} />
      ),
      drawerControllerLeft: (
        <ReactSVG src={drawerControllerLeft} style={style} className={className} onClick={onClick} />
      ),
      drawerControllerRight: (
        <ReactSVG src={drawerControllerRight} style={style} className={className} onClick={onClick} />
      ),
      descArrow: <ReactSVG src={descArrow} style={style} className={className} />,
      ascArrow: <ReactSVG src={ascArrow} style={style} className={className} />,
      appLoader: (
        <ReactSVG
          src={appLoader}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      
    signOut: (
        <ReactSVG
          src={signOut}
          style={style}
          className={className}
          onClick={onClick}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      userPlaceholder: (
        <ReactSVG
          src={userPlaceholder}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      editIcon25px: (
        <ReactSVG
          src={editIcon25px}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      upload: (
        <ReactSVG src={upload} style={style} onClick={onClick} className={className} />
      ),
      upload2: (
        <ReactSVG src={upload2} style={style} className={className} />
      ),
      closeSquare: (
        <ReactSVG onMouseDown={onMouseDown} src={closeSquare} style={style} className={className}/>
      ),
      addWhite: (
        <ReactSVG src={addWhite} style={style} className={className} />
      ),
      toastError: (
        <ReactSVG src={toastError} style={style} className={className} />
      ),
      toastSuccess: (
        <ReactSVG src={toastSuccess} style={style} className={className} />
      ),
      closeCircle: (
        <ReactSVG src={closeCircle} style={style} className={className} />
      ),
      filter: <ReactSVG src={filter} style={style} className={className} />,
      searchNormal: (
        <ReactSVG src={searchNormal} style={style} className={className} />
      ),
      checkSquare: (
        <ReactSVG
          src={checkSquare}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      uncheckSquare: (
        <ReactSVG
          src={uncheckSquare}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
          style={style}
          className={className}
        />
      ),
      alertOctagon: (
        <ReactSVG src={alertOctagon} style={style} className={className} />
      ),
  
      editIcon: (
        <ReactSVG
          src={editIcon}
          style={style}
          className={className}
          onClick={onClick}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      radioButtonChecked: (
        <ReactSVG src={radioButtonChecked} style={style} className={className} />
      ),
      radioButtonUnchecked: (
        <ReactSVG
          src={radioButtonUnchecked}
          style={style}
          className={className}
        />
      ),
  
      trash: (
        <ReactSVG
          src={trash}
          style={style}
          className={className}
          onClick={onClick}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      trashBlack: (
        <ReactSVG
          src={trashBlack}
          style={style}
          className={className}
          onClick={onClick}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
      
    grayLeftArrowDisabled: (
        <ReactSVG
          src={grayLeftArrowDisabled}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
  
      grayLeftArrowEnabled: (
        <ReactSVG
          src={grayLeftArrowEnabled}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
  
      grayRightArrowDisabled: (
        <ReactSVG
          src={grayRightArrowDisabled}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),
  
      grayRightArrowEnabled: (
        <ReactSVG
          src={grayRightArrowEnabled}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),

    arrowDownBlack: (
      <ReactSVG
        src={arrowDownBlack}
        style={style}
        className={className}
        beforeInjection={(svg) => {
          svg.setAttribute('style', svgStyle);
        }}
      />
    ),

    roundedLeftArrowBlack: (
      <ReactSVG
        src={roundedLeftArrowBlack}
        style={style}
        className={className}
        beforeInjection={(svg) => {
          svg.setAttribute('style', svgStyle);
        }}
      />
    ),

    roundedLeftArrowGray: (
      <ReactSVG
        src={roundedLeftArrowGray}
        style={style}
        className={className}
        beforeInjection={(svg) => {
          svg.setAttribute('style', svgStyle);
        }}
      />
    ),

    roundedRightArrowBlack: (
      <ReactSVG
        src={roundedRightArrowBlack}
        style={style}
        className={className}
        beforeInjection={(svg) => {
          svg.setAttribute('style', svgStyle);
        }}
      />
    ),

    roundedRightArrowGray: (
      <ReactSVG
        src={roundedRightArrowGray}
        style={style}
        className={className}
        beforeInjection={(svg) => {
          svg.setAttribute('style', svgStyle);
        }}
      />
    ),
    notificationBing: (
        <ReactSVG
          src={notificationBing}
          style={style}
          className={className}
          beforeInjection={(svg) => {
            svg.setAttribute('style', svgStyle);
          }}
        />
      ),

   
  };

  return <>{customer_icon[name]}</>;
};

export default CustomIcon;
