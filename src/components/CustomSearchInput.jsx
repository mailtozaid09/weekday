import React from 'react';
import { TextField, Box, OutlinedInput, Typography } from '@mui/material';
import { addItemsTableTextFieldSX, typographyConstants } from '../resources/theme/constants';
import useStyles from './styles';

const CustomSearchInput = ({ 
    title,
    type = 'text',
    placeholder,
    onChange,
    value,
    width,
    autoFocus,
    InputProps = {},
    readOnly = false,
    keyPressed = null,
    minWidth = 220 
}) => {

    const classes = useStyles();

    return (
        <Box sx={{display: 'flex', flexDirection: 'column', width: 'fit-content'}} >
            <Typography variant={typographyConstants.caption} >
                {title}
            </Typography>
            <OutlinedInput
                size='small'
                fullWidth={true}
                multiline
                type={'text'}
                autoFocus={autoFocus}
                sx={{ 
                    ...addItemsTableTextFieldSX, 
                    width: width,
                    maxWidth: 200,
                    maxHeight: 50,
                    overflow: 'auto',
                    border: '0.5px solid black'
                }}
                placeholder={placeholder}
                onChange={onChange}
                value={value}
                inputProps={InputProps}
                readOnly={readOnly}
                disabled={readOnly}
                onKeyPress={type === 'number' || type === 'tel' ? keyPressed : undefined}
            />
        </Box>
    );
};

export default CustomSearchInput;
