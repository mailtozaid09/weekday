export const getScreenWidthAndHeight = () => {
  let windowHeight = window.innerHeight;
  let windowWidth = window.innerWidth / 2;

  let scaleVariable = window.screen.availWidth / window.innerWidth;

  if (scaleVariable < 0.85) {
    windowHeight = windowHeight * scaleVariable;
    windowWidth = windowWidth * scaleVariable;
  }

  if (scaleVariable > 0.85 && scaleVariable < 1.15) {
    windowHeight = window.innerHeight;
    windowWidth = window.innerWidth / 2;
  }

  if (scaleVariable > 1.15) {
    windowHeight = windowHeight * scaleVariable;
    windowWidth = windowWidth * scaleVariable;
  }
  return [windowHeight, windowWidth];
};

export const getTablewidth = (sideMenuWidth, drawerCollapsed) => {
  let windowWidth = 'calc(100vw - ' + sideMenuWidth + ')';
  let scaleVariable = window.screen.availWidth / window.innerWidth;
  let newWindowWidth = '';

  if (drawerCollapsed) {
    if (scaleVariable < 2) {
      newWindowWidth = windowWidth;
    } else if (scaleVariable >= 2) {
      newWindowWidth = '100vw';
    }
  } else {
    if (scaleVariable < 1.75) {
      newWindowWidth = windowWidth;
    } else if (scaleVariable >= 1.75) {
      newWindowWidth = '100vw';
    }
  }

  return newWindowWidth;
};
