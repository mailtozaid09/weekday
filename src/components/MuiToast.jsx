import React, { useState } from 'react';
import useStyles from './styles';
import { Box, Card, Typography, IconButton, Snackbar } from '@mui/material';
import CustomIcon from './CustomIcon';
import {
  iconConstants,
  typographyConstants,
} from '../resources/theme/constants';
import Slide from '@mui/material/Slide';


export default function MuiToast({ subTitle, title, isSuccess, anchorOrigin }) {
  const showSuccess = isSuccess ?? true;
  const classes = useStyles();

  const [open, setOpen] = useState(true);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  return (
    <Snackbar
      open={open}
      anchorOrigin={anchorOrigin ?? { vertical: 'bottom', horizontal: 'right' }}
      autoHideDuration={3000}
      onClose={handleClose}
      TransitionComponent={Slide}
    >
      <Card
        sx={{ width: '444px', borderRadius: '10px', padding: '20px 10px' }}
        className={classes.flexRowSpaceBetween}
      >
        <Box className={classes.flexColumnCenter}>
          {showSuccess ? (
            <CustomIcon
              name={toastSuccess}
              style={{ height: '36px', width: '36px' }}
            />
          ) : (
            <CustomIcon
              name={toastSuccess}
              style={{ height: '36px', width: '36px' }}
            />
          )}
        </Box>
        <Box
          sx={{
            width: '100%',
            ml: '23px',
          }}
        >
          {title && (
            <Typography
              variant={typographyConstants.h6}
              sx={{
                display: '-webkit-box',
                overflow: 'hidden',
                WebkitBoxOrient: 'vertical',
                WebkitLineClamp: 2,
                mt: '6px',
                wordBreak: 'break-word',
              }}
            >
              {title}
            </Typography>
          )}
          {subTitle && (
            <Typography
              variant={typographyConstants.body3}
              sx={{
                display: '-webkit-box',
                overflow: 'hidden',
                WebkitBoxOrient: 'vertical',
                WebkitLineClamp: 5,
                mt: '6px',
                wordBreak: 'break-word',
              }}
            >
              {subTitle}
            </Typography>
          )}
        </Box>
        <Box sx={{ ml: '13px' }}>
          <Box className={classes.flexColumnCenter}>
            <IconButton onClick={handleClose}>
              <CustomIcon
                name={iconConstants.closeCircle}
                style={{ height: '36px', width: '36px' }}
              />
            </IconButton>
          </Box>
        </Box>
      </Card>
    </Snackbar>
  );
}
