import React, { createContext, useState } from 'react';
import MuiToast from './MuiToast';

export const ToastContext = createContext();

export const ToastProvider = ({ children }) => {
  const [title, setTitle] = useState('');
  const [subTitle, setSubTitle] = useState('');
  const [anchorOrigin, setAnchorOrigin] = useState({});
  const [isSuccess, setIsSuccess] = useState(true);

  const showToast = (subTitle, title, anchorOrigin, isSuccess) => {
    setTitle(title);
    setSubTitle(subTitle);
    setAnchorOrigin(anchorOrigin);
    setIsSuccess(isSuccess);
    setTimeout(() => {
      setTitle('');
      setSubTitle('');
    }, 3000);
  };

  return (
    <ToastContext.Provider value={{ showToast }}>
      {children}
      {(title || subTitle) && (
        <MuiToast
          subTitle={subTitle}
          title={title}
          anchorOrigin={anchorOrigin}
          isSuccess={isSuccess}
        />
      )}
    </ToastContext.Provider>
  );
};
