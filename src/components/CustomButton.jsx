// CustomButton.js
import React from 'react';
import Button from '@mui/material/Button';

export default function CustomButton({ children, ...props }) {
  return (
    <Button variant="contained" {...props}>
      {children}
    </Button>
  );
}
