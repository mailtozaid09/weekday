import * as React from 'react';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import { linkConstants, typographyConstants } from '../resources/theme/constants';
import { capitalize } from '@mui/material';

function LinkRouter(props) {
  return <Link {...props} component={RouterLink} />;
}

const RouterBreadcrumbs = ({ displayText }) => {
  const location = useLocation();

  const pathnames = location.pathname.split('/').filter((x) => x);
  let isRetailer = false;

  return (
    <Breadcrumbs
      aria-label='breadcrumb'
      separator={<NavigateNextIcon fontSize='small' />}
    >
      {pathnames.length === 0 && (
        <Typography
          color='text.primary'
          variant={typographyConstants.subtitle1}
        >
          {linkConstants.link1}
        </Typography>
      )}

      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        const to = `/${pathnames.slice(0, index + 1).join('/')}`;
        let originalString = value;

        return last ? (
          <Typography
            color='text.primary'
            key={to}
            variant={typographyConstants.subtitle1}
          >
            {capitalize(originalString)}
          </Typography>
        ) : (
          <LinkRouter underline='hover' color='inherit' to={to} key={to}>
            {capitalize(originalString)}
          </LinkRouter>
        );
      })}
    </Breadcrumbs>
  );
};

export default RouterBreadcrumbs;
