import React, { useState } from 'react';
import { Box, Select, MenuItem, ListItemText, Checkbox, Chip, InputLabel, Typography } from '@mui/material';
import CustomIcon from './CustomIcon'; 
import { iconConstants, addItemsTableSelectFieldSX, typographyConstants } from '../resources/theme/constants';

import useStyles from './styles';

const CustomSelect = ({ dataArray, title, minWidth = 220, selectedValue, handleChange, handleDelete }) => {
    const classes = useStyles();
    // const [selectedRole, setSelectedRole] = useState('');

    // const handleRoleChange = (event) => {
    //     const newValue = event.target.value;
    //     setSelectedRole(newValue);
    // };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: '226px',
            },
        },
    };

    return (
        <Box sx={{display: 'flex', flexDirection: 'column', width: 'fit-content'}} >
            <Typography variant={typographyConstants.caption} >
                {title}
            </Typography>
            <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                className={classes.textField}
                size='small'
                autoFocus={false}
                value={selectedValue}
                onChange={(e) => handleChange(e)}
                sx={{
                    ...addItemsTableSelectFieldSX,
                    minWidth: minWidth,
                    border: '0.5px solid black'
                }}
                renderValue={(selected) => (
                    <Chip
                        key={selected}
                        sx={{
                            backgroundColor: 'globalElementColors.canvas2',
                            height: '26px',
                            borderRadius: '5px',
                            padding: '2px 5px',
                        }}
                        label={selected}
                        clickable
                        deleteIcon={
                            <CustomIcon
                                name={iconConstants.closeSquare}
                                onMouseDown={(e) => {
                                    e.stopPropagation();
                                    handleDelete();
                                }}
                            />
                        }
                        onDelete={(e) => {
                            e.stopPropagation();
                        }}
                    />
                )}
                MenuProps={MenuProps}
            >
                {dataArray.map((value) => (
                    <MenuItem
                        className={classes.textField}
                        key={value}
                        value={value}
                        sx={{
                            '&.Mui-selected': {
                                backgroundColor: (theme) =>
                                    theme.palette.globalElementColors.white,
                            },
                            '&.Mui-selected:hover': {
                                backgroundColor: (theme) =>
                                    theme.palette.globalElementColors.canvas1,
                            },
                        }}
                    >
                        <ListItemText primary={value} />
                    </MenuItem>
                ))}
            </Select>
        </Box>
    );
};

export default CustomSelect;
