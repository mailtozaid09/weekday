export const apiRequest = {
    get: 'get',
    patch: 'patch',
    post: 'post',
    put: 'put',
    delete: 'delete',
};
export const apiHeaders = {
    contentType: 'content-type',
    authorization: 'authorization',
    typeFormData: 'multipart/form-data',
    acceptType: 'application/json',
    deviceType: 'device-type',
    web: 'Web',
    klipAuthToken: 'klip-auth-token',
    language: 'language',
    timezone: 'timezone',
    today: 'today',
    appVersion: 'appversion',
    deviceDetails: 'devicedetails',
    deviceUdid: 'device-udid',
};

export const endpoints = {
    base_url: 'https://api.weekday.technology/adhoc',
}


export const pathUrls = {
    get_all_jobs: '/getSampleJdJSON',
};

export const requestParams = {
    versionNo: 'versionNo',
    profilePicture: 'profilePicture',
    key: 'key',
    value: 'value',

    token: 'token',
    password: 'password',
    email: 'email',
    deviceType: 'device-type',
    web: 'Web',
    oldPassword: 'oldPassword',
    newPassword: 'newPassword',

    filter: 'filter',
    status: 'status',
    roles: 'roles',
    days: 'days',
    startDate: 'startDate',
    endDate: 'endDate',

    permissions: 'permissions',
    users: 'users',

    fullName: 'fullName',
    reason: 'reason',

    ids: 'ids',
    retailerId: 'retailerId',

    customerId: 'customerId',
    phone: 'phone',
    customerId: 'customerId',
    number: 'number',
    countryCode: 'countryCode',
    referenceno: 'referenceno',
    enableEncryption: 'enableEncryption',

    userTimeZone: 'userTimeZone',
    userTimeStamp: 'userTimeStamp',
    category: 'category',
    description: 'description',
    updatedAt: 'updatedAt',
    ticketId: 'ticketId',
    reply: 'reply',
    ticketIds: 'ticketIds',
};
