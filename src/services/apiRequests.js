import { apiHeaders, pathUrls, requestParams } from './apiConstants';

import authFetch from './axios';


//  APIS

//-------------- Get API ------------
export const getAllJobs = async( 
    body, onSuccess, onError, 
) => {
    return authFetch
        .post(pathUrls.get_all_jobs, body)
        .then((res) => {
            onSuccess(res);
        },
        (err) => {
            onError(err);
        }
    );
};
